var api_url = 'http://isaac.metajua.gq/api.php';

var current_task = {
    id: null,
    name: null,
    description: null
};

var db = window.openDatabase( 'todoApp', '0.0.1', 'Todo App', 100000 );

function createTable() {
	db.transaction( function( tx ) {
		tx.executeSql(
			'CREATE TABLE tasks ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER, name CHAR(250), description TEXT, start_datetime DATETIME, end_datetime DATETIME, status INTEGER, last_updated DATETIME )',
			[],
			function( tx, result ) {
				window.localStorage.setItem( 'table_created', true );
			},
			null
		);
	}, null );
}

function getTimestamp( date ) {
	date = typeof date == 'undefined' ? ( new Date() ) : date;

	var timestamp = '' + date.getFullYear(),
		month = ( date.getMonth() + 1 ),
		day = date.getDate(),
		hours = date.getHours(),
		minutes = date.getMinutes(),
		seconds = date.getSeconds();

	timestamp += '-' + ( month < 10 ? '0' + month : month ) + '-' + ( day < 10 ? '0' + day : day );
	timestamp += ' ' + ( hours < 10 ? '0' + hours : hours ) + ':' + ( minutes < 10 ? '0' + minutes : minutes ) + ':' + ( seconds < 10 ? '0' + seconds : seconds );

	return timestamp;
}

function synchronizeData() {
	db.transaction( function( tx ) {
		tx.executeSql(
			'SELECT * FROM tasks WHERE remote_id IS NULL LIMIT 1',
			[],
			function( tx, result ) {
				if ( result.rows.length > 0 ) {
					var record = result.rows.item( 0 );

					$.post(
						'http://isaac.metajua.gq/api.php',
						{
							action: 'add_task',
							name: record.name,
							description: record.description,
							start_datetime: record.start_datetime,
							end_datetime: record.end_datetime
						},
						function( response ) {
							if ( response.result == 'successful' )
								db.transaction( function( tx ) {
									tx.executeSql(
										'UPDATE tasks SET remote_id = ? WHERE id = ?',
										[ response.remote_id, record.id ],
										function( tx, result ) {
											synchronizeData();
										},
										function( tx, error ) {
											synchronizeData();
										}
									);
								}, null );

							else
								synchronizeData();
						},
						'json'
					).fail( function() {
						synchronizeData();
					} );
				}

				else
					synchronizeData();
			},
			null
		);
	}, null );
}

document.addEventListener( 'deviceready', function() {
	if ( window.localStorage.getItem( 'table_created' ) == null )
		createTable();

	synchronizeData();
} );



$( '#tasks' ).on( 'pagecreate', function() {
	$( '#tasks [data-role="header"] a' ).removeClass( 'ui-btn' );
} );


$( '#tasks' ).on( 'pagebeforeshow', function() {
	db.transaction( function( tx ) {
		tx.executeSql(
			'SELECT * FROM tasks ORDER BY id DESC',
			[],
			function( tx, result ) {
				var html = '';

				for ( i = 0; i < result.rows.length; i++ ) {
					var record = result.rows.item( i );

					html += '<li>' +
						'<a href="#task_details" data-task-id="' + record.id + '">' +
							record.name +
						'</a>' +
					'</li>';
				}

				$( '#tasks_list' ).html( html ).listview( 'refresh' );
			},
			null
		);
	}, null );
} );

$( '#add_task_save' ).off( 'click' ).on( 'click', function( event ) {
	event.preventDefault();

	var name = $( '#add_task_name' );
	var description = $( '#add_task_description' );

	if ( name.val() != '' ) {
		db.transaction( function( tx ) {
			var now = new Date();
				var start_datetime = getTimestamp( now );

			now.setDate( now.getDate() + 2 );
				var end_datetime = getTimestamp( now );

			tx.executeSql(
				'INSERT INTO tasks ( name, description, start_datetime, end_datetime, status, last_updated ) VALUES ( ?, ?, ?, ?, ?, ? )',
				[ name.val(), description.val(), start_datetime, end_datetime, 0, start_datetime ],
				function( tx, result ) {
					$.mobile.changePage( "#tasks", {
						transition: "pop",
						reverse: false,
						changeHash: false
					} );

					name.val( '' );
					description.val( '' );
				},
				null
			);
		}, null );
	}

	else {
		name.focus();
	}
} );
